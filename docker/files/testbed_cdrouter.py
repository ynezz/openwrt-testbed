#!/usr/bin/env python3

import re
import argparse
import json
import os
import sys
import time
import logging
import humanize

from types import SimpleNamespace
from http.client import RemoteDisconnected
from requests.exceptions import ConnectionError

from cdrouter import CDRouter
from cdrouter.jobs import Job, Options
from cdrouter.configs import Config
from cdrouter.cdr_error import CDRouterError

from gitlab import Gitlab
from gitlab.exceptions import GitlabCreateError, GitlabUpdateError
from gitlab.exceptions import GitlabAuthenticationError, GitlabGetError

GITLAB_API_URL = os.environ.get("GITLAB_API_URL")
GITLAB_API_TOKEN = os.environ.get("GITLAB_API_TOKEN")
GITLAB_PROJECT = os.environ.get("GITLAB_PROJECT")


class GitLabHelper(Gitlab):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def login(self):
        try:
            self.auth()
        except GitlabAuthenticationError:
            return False

        return True

    def project_get(self, project_path):
        p = None

        try:
            p = self.projects.get(project_path)
        except GitlabGetError as e:
            if e.response_code != 404:
                raise

        return p

    def project_set(self, project_path):
        p = self.project_get(project_path)
        if not p:
            return False

        self.active_project = p
        return True

    def issue_get(self, **kwargs):
        f = self.active_project.issues.list(**kwargs)
        if not f:
            return

        title = kwargs.get("title")
        for issue in f:
            if issue.title == title:
                return issue

    def issue_notes_create(self, issue, body):
        try:
            issue.notes.create({"body": body})
        except GitlabCreateError as e:
            logging.error("issue_create exception:", e)
            return False

        return True

    def issue_create(self, **kwargs):
        new_issue = {
            "title": kwargs.get("title", ""),
            "description": kwargs.get("description", ""),
            "labels": kwargs.get("labels", ""),
        }

        try:
            self.active_project.issues.create(new_issue)
        except GitlabCreateError as e:
            logging.error("issue_create exception:", e)
            return False

        return True


class OpenWrtSystemInfo:
    def __init__(self, filename):
        with open(filename, "r") as read_file:
            self.data = json.load(read_file)

    @property
    def kernel(self):
        return self.data["kernel"]

    @property
    def board_name(self):
        n = self.data["board_name"]
        return n.replace(",", "-")

    @property
    def target(self):
        return self.data["release"]["target"]

    @property
    def distribution(self):
        return self.data["release"]["distribution"]

    @property
    def revision(self):
        return self.data["release"]["revision"]

    @property
    def version(self):
        return self.data["release"]["version"]

    def sanitize(self, s):
        return s.replace("+", "-").replace("/", "-")

    def as_tags(self):
        return self.sanitize(
            "kernel_{},board_{},target_{},revision_{},version_{},distro_{}".format(
                self.kernel,
                self.board_name,
                self.target,
                self.revision,
                self.version,
                self.distribution,
            )
        )


class TestbedCDRouter:
    def __init__(self, args):
        self.args = args
        self.configs_path = os.path.join(self.args.root_dir, "configurations")
        self.packages_path = os.path.join(self.args.root_dir, "packages")

    def connect(self):
        api_token = os.getenv("CDROUTER_API_TOKEN")
        api_url = os.getenv("CDROUTER_API_URL")

        if not api_token or not api_url:
            logging.error("API token or URL is missing")
            exit(1)

        logging.debug("Connecting to {}".format(api_url))
        self.cdr = CDRouter(api_url, token=api_token)

    def job_launch(self):
        p = self.cdr.packages.get_by_name(self.args.package_name)
        logging.info(
            "Using test package '{name}' with id {id} and {test_count} tests defined.".format(
                name=p.name, id=p.id, test_count=p.test_count
            )
        )

        p = self.cdr.packages.get(p.id)
        config = self.cdr.configs.get_by_name(self.args.configuration)
        device = self.cdr.devices.get_by_name(self.args.device)
        if config:
            p.config_id = config.id
        if device:
            p.device_id = device.id
        if config or device:
            self.cdr.packages.edit(p)

        a = self.cdr.packages.analyze(p.id)
        self.job_total = a.run_count
        logging.info(
            "Package '{name}' will run {run_count} tests and skip {skipped_count} tests.".format(
                name=p.name, run_count=a.run_count, skipped_count=a.skipped_count
            )
        )

        for test in a.skipped_tests:
            logging.info(
                "Skipping test '{synopsis}' due to '{skip_name}'.".format(
                    synopsis=test.synopsis, skip_name=test.skip_name
                )
            )

        tags = None
        if self.args.system_info:
            tags = OpenWrtSystemInfo(self.args.system_info).as_tags().split(",")

        if self.args.tags:
            tags += self.args.tags.split(",")

        options = Options(tags=tags)
        job = Job(package_id=p.id, options=options)
        job = self.cdr.jobs.launch(job)
        while job.result_id is None:
            time.sleep(1)
            job = self.cdr.jobs.get(job.id)

        self.job = job

    def job_progress(self):
        current = None
        job = self.cdr.jobs.get(self.job.id)
        while job.status == "running":

            updates = self.cdr.results.updates(job.result_id)
            running = updates.running
            progress = updates.progress

            if not running or not progress:
                job = self.cdr.jobs.get(self.job.id)
                continue

            description = running.description
            if current == description:
                job = self.cdr.jobs.get(self.job.id)
                continue

            current = description

            logging.info(
                "Running test {finished}/{total} ({percent}%) '{description}'".format(
                    description=description,
                    percent=progress.progress,
                    finished=progress.finished,
                    total=self.job_total,
                )
            )

            time.sleep(5)
            job = self.cdr.jobs.get(self.job.id)

    def job_result(self, job_id=None):
        job_id = job_id or self.job.result_id
        r = self.cdr.results.get(job_id)

        buf, filename = self.cdr.results.download_logdir_archive(job_id, format="tgz")
        logging.info("Exporting logdir archive {}".format(filename))
        with open(filename, "wb") as f:
            f.write(buf.getvalue())

        logging.info(
            "Test job finished as '{}' after {}.".format(
                r.status, humanize.naturaldelta(r.duration)
            )
        )
        logging.info(
            "Run {} tests, which {} failed and {} passed.".format(
                r.tests, r.fail, r.passed
            )
        )

        if r.fail == 0 and r.status == "completed":
            logging.info("Success!")
            exit(0)

        if r.fail == 0:
            exit(1)

        logging.error("{:=^50}".format(" [ FAILED TESTS ] "))
        for test in self.cdr.tests.iter_list(r.id, filter=["result=fail"]):
            logging.error("{} ({})".format(test.description, test.result))

        self.create_gitlab_issues(r.id)
        exit(1)

    def package_run(self):
        self.connect()
        self.job_launch()
        self.job_progress()
        self.job_result()

    def netif_available(self, name):
        for netif in self.cdr.system.interfaces():
            if netif.name == name:
                return True

        return False

    def wait_for_netif(self):
        self.connect()

        name = self.args.name
        timeout = time.time() + self.args.timeout

        while True:
            if self.netif_available(name):
                logging.info("Interface {} is available.".format(name))
                exit(0)

            if time.time() > timeout:
                logging.error(
                    "Interface {} is not available after {}".format(
                        name, humanize.naturaldelta(self.args.timeout)
                    )
                )
                exit(1)

            time.sleep(5)

    def package_stop(self):
        self.connect()

        f = ["status~(running|paused)"]
        pkg_name = self.args.package_name
        if pkg_name:
            f.append("package_name={}".format(pkg_name))

        for r in self.cdr.results.iter_list(filter=f):
            self.cdr.results.stop(r.id)
            logging.info(
                "Stopped '{}' package which was {}".format(r.package_name, r.status)
            )

    def package_export(self):
        self.connect()
        name = self.args.name
        p = self.cdr.packages.get_by_name(name)
        buf, filename = self.cdr.packages.export(p.id)
        filename = "{}.gz".format(self.args.filename or name)
        self.file_save(self.packages_path, buf.getvalue(), filename)

    def package_import(self):
        self.connect()
        archive = os.path.join(self.packages_path, self.args.filename)

        with open(archive, "rb+") as fd:
            si = self.cdr.imports.stage_import_from_file(fd)

        filename = re.sub(r"\.gz$", "", self.args.filename)
        name = self.args.name or filename

        req = self.cdr.imports.get_commit_request(si.id)
        for id in req.packages:
            p = req.packages[id]
            p.name = name
            logging.debug("Going to import package '{}'".format(name))
            p.should_import = True

        resp = self.cdr.imports.commit(si.id, req)
        for id in resp.packages:
            r = resp.packages[id].response
            if not r.imported:
                logging.error("Import of '{}' failed: {}".format(r.name, r.message))
                exit(1)

        logging.info(
            "Imported package '{}' from '{}'".format(r.name, self.args.filename)
        )

    def file_save(self, path, content, filename=None):
        filename = filename or self.args.filename
        os.makedirs(path, exist_ok=True)
        dest = os.path.join(path, filename)
        with open(dest, "wb") as f:
            f.write(content)

    def config_export(self):
        self.connect()
        c = self.cdr.configs.get_by_name(self.args.name)
        content = self.cdr.configs.get_plaintext(c.id)
        self.file_save(self.configs_path, content.encode())

    def file_content(self, path, filename=None):
        content = None
        filename = filename or self.args.filename
        src = os.path.join(path, filename)
        with open(src, "rb") as f:
            content = f.read()
        return content

    def config_check(self):
        self.connect()
        content = self.file_content(self.configs_path)
        check = self.cdr.configs.check_config(content)
        if not check.errors:
            logging.info("OK, no errors!")
            exit(0)

        for error in check.errors:
            logging.error(
                "{}: {}: {}".format(
                    self.args.filename, ",".join(error.lines), error.error
                )
            )

        exit(1)

    def config_import(self):
        self.connect()
        name = self.args.name or self.args.filename

        try:
            c = self.cdr.configs.get_by_name(name)
            self.cdr.configs.delete(c.id)
            logging.debug("Deleted already existing config '{}'".format(c.name))
        except CDRouterError:
            pass

        content = self.file_content(self.configs_path)
        config = Config(contents=content, name=name)
        self.cdr.configs.create(config)
        logging.info("Imported config '{}' from '{}'".format(name, self.args.filename))

    def cmd_create_gitlab_issues(self):
        self.create_gitlab_issues(self.args.result_id)

    def create_gitlab_issue(self, glab, result, test):
        logs = []
        headers = []
        logging.debug("Getting {}'s result test {}".format(result.id, test.seq))

        for line in self.cdr.tests.iter_list_log(result.id, test.seq, limit=5000):
            if line.header and not line.message.startswith("---"):
                headers.append(line.message)

            # FIXME: workaround for some API/library filter bug
            if line.prefix in ["WARNING", "FAIL", "ALERT", "NOTICE", "SECTION", "PASS"]:
                logs.append(line.raw)

            # FIXME: workaround for some API/library bug
            if line.line > 5000:
                break

        d = {
            "logs": logs,
            "headers": headers,
            "title": test.description,
            "name": test.name,
        }
        self.handle_gitlab_issue_for_test(glab, result, SimpleNamespace(**d))

    def create_gitlab_issues(self, result_id):
        self.connect()

        result = self.cdr.results.get(result_id)

        logging.info("Creating GitLab issues for {} result".format(result_id))

        glab = GitLabHelper(
            GITLAB_API_URL, private_token=GITLAB_API_TOKEN, api_version=4
        )
        if not glab.login():
            sys.exit("GitLab login failed")

        if not glab.project_set(GITLAB_PROJECT):
            sys.exit("GitLab project {} doesn't exist".format(GITLAB_PROJECT))

        num_retries = 3
        for test in self.cdr.tests.iter_list(
            result.id, filter=["loop=1", "result=fail"], sort="-seq"
        ):
            while num_retries > 0:
                try:
                    self.create_gitlab_issue(glab, result, test)
                    break
                except (RemoteDisconnected, ConnectionError):
                    logging.debug("Got exception, retrying...")
                    time.sleep(5)
                    num_retries -= 1

    def update_gitlab_issue(self, title, issue, labels):
        issue_labels = issue.labels
        new_labels = set(issue_labels) | set(labels)
        diff_labels = new_labels.difference(issue_labels)

        if not diff_labels:
            return

        issue.labels += labels

        num_retries = 3
        while num_retries > 0:
            try:
                issue.save()
                break
            except GitlabUpdateError:
                logging.debug("Got exception, retrying...")
                time.sleep(5)
                num_retries -= 1

        note = self.issue_comment(labels)
        issue.notes.create({"body": note})

        logging.info("Updated '{}' issue".format(title))

    def handle_gitlab_issue_for_test(self, glab, result, test):
        logging.debug("Processing GitLab issues for {}".format(test.title))

        labels = []
        for label in result.tags:
            labels.append(label.replace("_", ":"))

        cdr_test = "cdr-test:{}".format(test.name)
        labels.append(cdr_test)
        labels.append("cdr-result:{}".format(result.id))
        labels.append(self.kernel_minor_label(labels))

        test_suite = self.cdr.testsuites.get_test(test.name)
        cdr_module = "cdr-module:{}".format(test_suite.module)
        labels.append(cdr_module)

        dhcp_re = re.compile(r"DHCP")
        security_re = re.compile(r"(security|firewall)", re.IGNORECASE)
        md = self.test_result_to_markdown(test, result.id)

        labels_str = str.join(" ", labels)
        lookup = md.title + md.content + labels_str + test.name + test_suite.module
        if re.search(security_re, lookup):
            labels.append("topic:security-firewall")
            labels.append("priority:high")

        if re.search(dhcp_re, lookup):
            labels.append("topic:dhcp")

        match_labels = [cdr_test, cdr_module]
        issue = glab.issue_get(state="opened", title=md.title, labels=match_labels)
        if issue:
            logging.debug("Going to update issue {}".format(md.title))
            return self.update_gitlab_issue(md.title, issue, labels)

        logging.debug("Going to create issue {}".format(md.title))

        glab.issue_create(title=md.title, description=md.content, labels=labels)
        new_issue = glab.issue_get(state="opened", title=md.title, labels=labels)
        if not new_issue:
            sys.exit("Unable to get created issue '{}'".format(md.title))

        note = self.issue_comment(labels)

        num_retries = 3
        while num_retries > 0:
            if glab.issue_notes_create(new_issue, note):
                logging.info("Created '{}' issue".format(md.title))
                return
            num_retries -= 1

        sys.exit("Unable to create issue '{}' note '{}'".format(md.title, note))

    def get_label_value(self, labels, prefix):
        v = [b.split(":")[1] for b in labels if b.startswith("{}:".format(prefix))]
        return v[0]

    def kernel_minor_label(self, labels):
        v = self.get_label_value(labels, "kernel")
        v = v.split(".")
        return "kernel:{}.{}".format(v[0], v[1])

    def issue_comment(self, labels):
        job_id = os.getenv("CI_JOB_ID", "")
        job_url = os.getenv("CI_JOB_URL", "")
        board = self.get_label_value(labels, "board")
        comment = """
        [Artifacts]({job_url}/artifacts/browse) for
        failed [job #{job_id}]({job_url}) for **{board}** board.
        """.strip().format(
            job_url=job_url, job_id=job_id, board=board
        )
        return comment

    def test_result_to_markdown(self, test, result):
        desc = []
        hdrs = []

        for h in test.headers:
            if h.startswith("Test cdrouter"):
                continue
            if h.startswith("Description:"):
                continue

            if h.startswith("Module:"):
                hdrs.append(h.replace("Module:", "**Module:**"))
            elif h.startswith("Name:"):
                hdrs.append(h.replace("Name:", "**Name:**"))
            else:
                desc.append(h)

        content_fmt = """
**CDRouter result:** {result} {hdrs}
#### Test description
{desc}
#### Fail log
```
{logs}
```
"""
        d = {
            "title": "Test `{}` failed".format(test.title),
            "content": content_fmt.format(
                result=result,
                hdrs=str.join("", hdrs),
                desc=str.join("\n", desc),
                logs=str.join("\n", test.logs),
            ),
        }
        return SimpleNamespace(**d)


def main():
    logging.basicConfig(
        level=logging.INFO, format="%(levelname)7s: %(message)s", stream=sys.stderr
    )

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-r",
        "--root-dir",
        type=str,
        default=os.environ.get("TB_CDROUTER_ROOT", ".testbed/cdrouter"),
        help="CDRouter root directory (default: %(default)s)",
    )
    parser.add_argument("-d", "--debug", action="store_true", help="enable debug mode")

    subparsers = parser.add_subparsers(dest="command", title="available subcommands")
    subparser = subparsers.add_parser("package_run", help="run package")
    subparser.add_argument("package_name", help="name of the testing package")
    subparser.add_argument("-t", "--tags", help="additional tags for the result")
    subparser.add_argument("-d", "--device", help="device used for testing")
    subparser.add_argument(
        "-c", "--configuration", help="configuration used for testing"
    )
    subparser.add_argument(
        "-s",
        "--system-info",
        help="JSON file with system information for additional tags",
    )
    subparser.set_defaults(func=TestbedCDRouter.package_run)

    subparser = subparsers.add_parser(
        "package_stop", help="stop running/paused package"
    )
    subparser.add_argument(
        "-p", "--package-name", help="name of the package (default: any)"
    )
    subparser.set_defaults(func=TestbedCDRouter.package_stop)

    subparser = subparsers.add_parser("package_export", help="export package")
    subparser.add_argument("name", help="package name")
    subparser.add_argument(
        "-f", "--filename", help="destination filename (default: name)"
    )
    subparser.set_defaults(func=TestbedCDRouter.package_export)

    subparser = subparsers.add_parser("package_import", help="package configuration")
    subparser.add_argument("filename", help="package filename")
    subparser.add_argument("-n", "--name", help="package name, (default: filename)")
    subparser.set_defaults(func=TestbedCDRouter.package_import)

    subparser = subparsers.add_parser("config_export", help="export configuration")
    subparser.add_argument("name", help="configuration name")
    subparser.add_argument("filename", help="destination filename")
    subparser.set_defaults(func=TestbedCDRouter.config_export)

    subparser = subparsers.add_parser("config_check", help="check configuration")
    subparser.add_argument("filename", help="config filename")
    subparser.set_defaults(func=TestbedCDRouter.config_check)

    subparser = subparsers.add_parser("config_import", help="import configuration")
    subparser.add_argument("filename", help="config filename")
    subparser.add_argument("-n", "--name", help="config name, (default: filename)")
    subparser.set_defaults(func=TestbedCDRouter.config_import)

    subparser = subparsers.add_parser(
        "create_gitlab_issues", help="create GitLab issue for result"
    )
    subparser.add_argument("result_id", help="result id")
    subparser.set_defaults(func=TestbedCDRouter.cmd_create_gitlab_issues)

    subparser = subparsers.add_parser(
        "wait_for_netif", help="wait for network interface"
    )
    subparser.add_argument("name", help="interface name")
    subparser.add_argument(
        "-t",
        "--timeout",
        type=int,
        default=60,
        help="wait duration in seconds (default: %(default)s)",
    )
    subparser.set_defaults(func=TestbedCDRouter.wait_for_netif)

    args = parser.parse_args()
    if args.debug:
        logging.getLogger().setLevel(logging.DEBUG)

    if not args.command:
        print("command is missing")
        exit(1)

    cdr = TestbedCDRouter(args)
    args.func(cdr)


if __name__ == "__main__":
    main()
